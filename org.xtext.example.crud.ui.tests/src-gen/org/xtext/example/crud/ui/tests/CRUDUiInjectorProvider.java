/*
 * generated by Xtext 2.25.0
 */
package org.xtext.example.crud.ui.tests;

import com.google.inject.Injector;
import org.eclipse.xtext.testing.IInjectorProvider;
import org.xtext.example.crud.ui.internal.CrudActivator;

public class CRUDUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return CrudActivator.getInstance().getInjector("org.xtext.example.crud.CRUD");
	}

}
