package org.xtext.example.crud.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.crud.services.CRUDGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCRUDParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "'{'", "'}'", "':'", "'endpoint'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalCRUDParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCRUDParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCRUDParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCRUD.g"; }



     	private CRUDGrammarAccess grammarAccess;

        public InternalCRUDParser(TokenStream input, CRUDGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Models";
       	}

       	@Override
       	protected CRUDGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModels"
    // InternalCRUD.g:64:1: entryRuleModels returns [EObject current=null] : iv_ruleModels= ruleModels EOF ;
    public final EObject entryRuleModels() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModels = null;


        try {
            // InternalCRUD.g:64:47: (iv_ruleModels= ruleModels EOF )
            // InternalCRUD.g:65:2: iv_ruleModels= ruleModels EOF
            {
             newCompositeNode(grammarAccess.getModelsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModels=ruleModels();

            state._fsp--;

             current =iv_ruleModels; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModels"


    // $ANTLR start "ruleModels"
    // InternalCRUD.g:71:1: ruleModels returns [EObject current=null] : ( (lv_elements_0_0= ruleModel ) )* ;
    public final EObject ruleModels() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;



        	enterRule();

        try {
            // InternalCRUD.g:77:2: ( ( (lv_elements_0_0= ruleModel ) )* )
            // InternalCRUD.g:78:2: ( (lv_elements_0_0= ruleModel ) )*
            {
            // InternalCRUD.g:78:2: ( (lv_elements_0_0= ruleModel ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCRUD.g:79:3: (lv_elements_0_0= ruleModel )
            	    {
            	    // InternalCRUD.g:79:3: (lv_elements_0_0= ruleModel )
            	    // InternalCRUD.g:80:4: lv_elements_0_0= ruleModel
            	    {

            	    				newCompositeNode(grammarAccess.getModelsAccess().getElementsModelParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_elements_0_0=ruleModel();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getModelsRule());
            	    				}
            	    				add(
            	    					current,
            	    					"elements",
            	    					lv_elements_0_0,
            	    					"org.xtext.example.crud.CRUD.Model");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModels"


    // $ANTLR start "entryRuleModel"
    // InternalCRUD.g:100:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalCRUD.g:100:46: (iv_ruleModel= ruleModel EOF )
            // InternalCRUD.g:101:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalCRUD.g:107:1: ruleModel returns [EObject current=null] : (otherlv_0= 'model' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleAttribute ) )* ( (lv_endpoint_4_0= ruleEndpoint ) )* otherlv_5= '}' ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_attributes_3_0 = null;

        EObject lv_endpoint_4_0 = null;



        	enterRule();

        try {
            // InternalCRUD.g:113:2: ( (otherlv_0= 'model' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleAttribute ) )* ( (lv_endpoint_4_0= ruleEndpoint ) )* otherlv_5= '}' ) )
            // InternalCRUD.g:114:2: (otherlv_0= 'model' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleAttribute ) )* ( (lv_endpoint_4_0= ruleEndpoint ) )* otherlv_5= '}' )
            {
            // InternalCRUD.g:114:2: (otherlv_0= 'model' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleAttribute ) )* ( (lv_endpoint_4_0= ruleEndpoint ) )* otherlv_5= '}' )
            // InternalCRUD.g:115:3: otherlv_0= 'model' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleAttribute ) )* ( (lv_endpoint_4_0= ruleEndpoint ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getModelAccess().getModelKeyword_0());
            		
            // InternalCRUD.g:119:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCRUD.g:120:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCRUD.g:120:4: (lv_name_1_0= RULE_ID )
            // InternalCRUD.g:121:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getModelAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getModelRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalCRUD.g:141:3: ( (lv_attributes_3_0= ruleAttribute ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCRUD.g:142:4: (lv_attributes_3_0= ruleAttribute )
            	    {
            	    // InternalCRUD.g:142:4: (lv_attributes_3_0= ruleAttribute )
            	    // InternalCRUD.g:143:5: lv_attributes_3_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getAttributesAttributeParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_attributes_3_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_3_0,
            	    						"org.xtext.example.crud.CRUD.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalCRUD.g:160:3: ( (lv_endpoint_4_0= ruleEndpoint ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCRUD.g:161:4: (lv_endpoint_4_0= ruleEndpoint )
            	    {
            	    // InternalCRUD.g:161:4: (lv_endpoint_4_0= ruleEndpoint )
            	    // InternalCRUD.g:162:5: lv_endpoint_4_0= ruleEndpoint
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getEndpointEndpointParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_endpoint_4_0=ruleEndpoint();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"endpoint",
            	    						lv_endpoint_4_0,
            	    						"org.xtext.example.crud.CRUD.Endpoint");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getModelAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleAttribute"
    // InternalCRUD.g:187:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalCRUD.g:187:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalCRUD.g:188:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalCRUD.g:194:1: ruleAttribute returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )? ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_type_2_0=null;
        Token otherlv_3=null;
        EObject lv_data_4_0 = null;



        	enterRule();

        try {
            // InternalCRUD.g:200:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )? ) )
            // InternalCRUD.g:201:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )? )
            {
            // InternalCRUD.g:201:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )? )
            // InternalCRUD.g:202:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )?
            {
            // InternalCRUD.g:202:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalCRUD.g:203:4: (lv_name_0_0= RULE_ID )
            {
            // InternalCRUD.g:203:4: (lv_name_0_0= RULE_ID )
            // InternalCRUD.g:204:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_0_0, grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getColonKeyword_1());
            		
            // InternalCRUD.g:224:3: ( (lv_type_2_0= RULE_ID ) )
            // InternalCRUD.g:225:4: (lv_type_2_0= RULE_ID )
            {
            // InternalCRUD.g:225:4: (lv_type_2_0= RULE_ID )
            // InternalCRUD.g:226:5: lv_type_2_0= RULE_ID
            {
            lv_type_2_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_type_2_0, grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalCRUD.g:242:3: (otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+ )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==14) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalCRUD.g:243:4: otherlv_3= ':' ( (lv_data_4_0= ruleData ) )+
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_4); 

                    				newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getColonKeyword_3_0());
                    			
                    // InternalCRUD.g:247:4: ( (lv_data_4_0= ruleData ) )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==RULE_ID) ) {
                            int LA4_2 = input.LA(2);

                            if ( (LA4_2==EOF||LA4_2==RULE_ID||LA4_2==13||LA4_2==15) ) {
                                alt4=1;
                            }


                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalCRUD.g:248:5: (lv_data_4_0= ruleData )
                    	    {
                    	    // InternalCRUD.g:248:5: (lv_data_4_0= ruleData )
                    	    // InternalCRUD.g:249:6: lv_data_4_0= ruleData
                    	    {

                    	    						newCompositeNode(grammarAccess.getAttributeAccess().getDataDataParserRuleCall_3_1_0());
                    	    					
                    	    pushFollow(FOLLOW_10);
                    	    lv_data_4_0=ruleData();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"data",
                    	    							lv_data_4_0,
                    	    							"org.xtext.example.crud.CRUD.Data");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleEndpoint"
    // InternalCRUD.g:271:1: entryRuleEndpoint returns [EObject current=null] : iv_ruleEndpoint= ruleEndpoint EOF ;
    public final EObject entryRuleEndpoint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEndpoint = null;


        try {
            // InternalCRUD.g:271:49: (iv_ruleEndpoint= ruleEndpoint EOF )
            // InternalCRUD.g:272:2: iv_ruleEndpoint= ruleEndpoint EOF
            {
             newCompositeNode(grammarAccess.getEndpointRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEndpoint=ruleEndpoint();

            state._fsp--;

             current =iv_ruleEndpoint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEndpoint"


    // $ANTLR start "ruleEndpoint"
    // InternalCRUD.g:278:1: ruleEndpoint returns [EObject current=null] : (otherlv_0= 'endpoint' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleEndpoint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_type_1_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;


        	enterRule();

        try {
            // InternalCRUD.g:284:2: ( (otherlv_0= 'endpoint' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) ) )
            // InternalCRUD.g:285:2: (otherlv_0= 'endpoint' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) )
            {
            // InternalCRUD.g:285:2: (otherlv_0= 'endpoint' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) )
            // InternalCRUD.g:286:3: otherlv_0= 'endpoint' ( (lv_type_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getEndpointAccess().getEndpointKeyword_0());
            		
            // InternalCRUD.g:290:3: ( (lv_type_1_0= RULE_ID ) )
            // InternalCRUD.g:291:4: (lv_type_1_0= RULE_ID )
            {
            // InternalCRUD.g:291:4: (lv_type_1_0= RULE_ID )
            // InternalCRUD.g:292:5: lv_type_1_0= RULE_ID
            {
            lv_type_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_type_1_0, grammarAccess.getEndpointAccess().getTypeIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEndpointRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_11); 

            			newLeafNode(otherlv_2, grammarAccess.getEndpointAccess().getColonKeyword_2());
            		
            // InternalCRUD.g:312:3: ( (lv_value_3_0= RULE_STRING ) )
            // InternalCRUD.g:313:4: (lv_value_3_0= RULE_STRING )
            {
            // InternalCRUD.g:313:4: (lv_value_3_0= RULE_STRING )
            // InternalCRUD.g:314:5: lv_value_3_0= RULE_STRING
            {
            lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_3_0, grammarAccess.getEndpointAccess().getValueSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEndpointRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEndpoint"


    // $ANTLR start "entryRuleData"
    // InternalCRUD.g:334:1: entryRuleData returns [EObject current=null] : iv_ruleData= ruleData EOF ;
    public final EObject entryRuleData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleData = null;


        try {
            // InternalCRUD.g:334:45: (iv_ruleData= ruleData EOF )
            // InternalCRUD.g:335:2: iv_ruleData= ruleData EOF
            {
             newCompositeNode(grammarAccess.getDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleData=ruleData();

            state._fsp--;

             current =iv_ruleData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleData"


    // $ANTLR start "ruleData"
    // InternalCRUD.g:341:1: ruleData returns [EObject current=null] : ( (lv_value_0_0= RULE_ID ) ) ;
    public final EObject ruleData() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;


        	enterRule();

        try {
            // InternalCRUD.g:347:2: ( ( (lv_value_0_0= RULE_ID ) ) )
            // InternalCRUD.g:348:2: ( (lv_value_0_0= RULE_ID ) )
            {
            // InternalCRUD.g:348:2: ( (lv_value_0_0= RULE_ID ) )
            // InternalCRUD.g:349:3: (lv_value_0_0= RULE_ID )
            {
            // InternalCRUD.g:349:3: (lv_value_0_0= RULE_ID )
            // InternalCRUD.g:350:4: lv_value_0_0= RULE_ID
            {
            lv_value_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_value_0_0, grammarAccess.getDataAccess().getValueIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getDataRule());
            				}
            				setWithLastConsumed(
            					current,
            					"value",
            					lv_value_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleData"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000A010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000020L});

}