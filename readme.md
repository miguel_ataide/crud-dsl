## The CRUD Language

This is a Doman Specific Language (DSL) created using the Xtext framework. Its file extension is ".crud". Its goal is to create a user interface CRUD layer from easy writing. The syntax is based on the model concept, which is an entity that usually has a specific implementation for different levels, like database, API and user interface. This language allows the creation of a complete Front-End CRUD form based on a specified model.

## Source Code

The main custom files for this DSL are

- org.text.example.crud/src/org/xtext/example/crud/
	- CRUD.text
	- generator/CRUDGenerator.xtend
	- validation/CRUDValidator.java

## Generated Data

Based on the specified model, the DSL generates two files: "index.html" and "scripts.js". The metadata and data defined with the language syntax is smartly processed to generate the functional user interface.

- index.html
- scripts.js

## Generated Project Dependencies

- Bootstrap 4.6.1
- JQuery 3.6.0
- Font Awesome 4.7

## Grammar

The language grammar is defined as follows:

```xtext
model MODEL_NAME {
	ATTRIBUTE_NAME : ATTRIBUTE_TYPE : ATTRIBUTE_DATA
}
```


## Component Types

The are the following available types for the HTML5 components

- color
	- Is used for input fields that should contain a color. 
- date
	- Is used for input fields that should contain a date. 
- email
	- Is used for input fields that should contain an e-mail address. 
- file
	- Defines a file-select field and a "Browse" button for file uploads.
- number
	- Defines a numeric input field.
- password
	- Defines a password field.
- radio
	- Defines a radio button, which allows a user to select ONLY ONE of a limited number of choices.
- select
	- Defines a select field with the informed data. 
- text
	- Defines a single-line text input field.
- textarea
	- Defines a textarea input field. 
- time
	- Allows the user to select a time (no time zone).
- url
	- Is used for input fields that should contain a URL address.


For more details about HTML5 types, check [W3Schools](https://www.w3schools.com/html/html_form_input_types.asp). 

## Validation Rules

- Check for valid attribute types 
- Check for mandatory data on the types "radio" and "select"
- Check for valid endpoint types

## Example

Code:

```java
model Person{
	name : text
	email: email
	password : password
	gender : select : male female other
	nationality : radio : Brazilian Foreign
}
```

Result:

![Alt text](images/example.png?raw=true "Title")


