package org.xtext.example.crud.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.crud.services.CRUDGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCRUDParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "'{'", "'}'", "':'", "'endpoint'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalCRUDParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCRUDParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCRUDParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCRUD.g"; }


    	private CRUDGrammarAccess grammarAccess;

    	public void setGrammarAccess(CRUDGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModels"
    // InternalCRUD.g:53:1: entryRuleModels : ruleModels EOF ;
    public final void entryRuleModels() throws RecognitionException {
        try {
            // InternalCRUD.g:54:1: ( ruleModels EOF )
            // InternalCRUD.g:55:1: ruleModels EOF
            {
             before(grammarAccess.getModelsRule()); 
            pushFollow(FOLLOW_1);
            ruleModels();

            state._fsp--;

             after(grammarAccess.getModelsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModels"


    // $ANTLR start "ruleModels"
    // InternalCRUD.g:62:1: ruleModels : ( ( rule__Models__ElementsAssignment )* ) ;
    public final void ruleModels() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:66:2: ( ( ( rule__Models__ElementsAssignment )* ) )
            // InternalCRUD.g:67:2: ( ( rule__Models__ElementsAssignment )* )
            {
            // InternalCRUD.g:67:2: ( ( rule__Models__ElementsAssignment )* )
            // InternalCRUD.g:68:3: ( rule__Models__ElementsAssignment )*
            {
             before(grammarAccess.getModelsAccess().getElementsAssignment()); 
            // InternalCRUD.g:69:3: ( rule__Models__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCRUD.g:69:4: rule__Models__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Models__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getModelsAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModels"


    // $ANTLR start "entryRuleModel"
    // InternalCRUD.g:78:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalCRUD.g:79:1: ( ruleModel EOF )
            // InternalCRUD.g:80:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalCRUD.g:87:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:91:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalCRUD.g:92:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalCRUD.g:92:2: ( ( rule__Model__Group__0 ) )
            // InternalCRUD.g:93:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalCRUD.g:94:3: ( rule__Model__Group__0 )
            // InternalCRUD.g:94:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleAttribute"
    // InternalCRUD.g:103:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // InternalCRUD.g:104:1: ( ruleAttribute EOF )
            // InternalCRUD.g:105:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalCRUD.g:112:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:116:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // InternalCRUD.g:117:2: ( ( rule__Attribute__Group__0 ) )
            {
            // InternalCRUD.g:117:2: ( ( rule__Attribute__Group__0 ) )
            // InternalCRUD.g:118:3: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // InternalCRUD.g:119:3: ( rule__Attribute__Group__0 )
            // InternalCRUD.g:119:4: rule__Attribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleEndpoint"
    // InternalCRUD.g:128:1: entryRuleEndpoint : ruleEndpoint EOF ;
    public final void entryRuleEndpoint() throws RecognitionException {
        try {
            // InternalCRUD.g:129:1: ( ruleEndpoint EOF )
            // InternalCRUD.g:130:1: ruleEndpoint EOF
            {
             before(grammarAccess.getEndpointRule()); 
            pushFollow(FOLLOW_1);
            ruleEndpoint();

            state._fsp--;

             after(grammarAccess.getEndpointRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEndpoint"


    // $ANTLR start "ruleEndpoint"
    // InternalCRUD.g:137:1: ruleEndpoint : ( ( rule__Endpoint__Group__0 ) ) ;
    public final void ruleEndpoint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:141:2: ( ( ( rule__Endpoint__Group__0 ) ) )
            // InternalCRUD.g:142:2: ( ( rule__Endpoint__Group__0 ) )
            {
            // InternalCRUD.g:142:2: ( ( rule__Endpoint__Group__0 ) )
            // InternalCRUD.g:143:3: ( rule__Endpoint__Group__0 )
            {
             before(grammarAccess.getEndpointAccess().getGroup()); 
            // InternalCRUD.g:144:3: ( rule__Endpoint__Group__0 )
            // InternalCRUD.g:144:4: rule__Endpoint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Endpoint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEndpointAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEndpoint"


    // $ANTLR start "entryRuleData"
    // InternalCRUD.g:153:1: entryRuleData : ruleData EOF ;
    public final void entryRuleData() throws RecognitionException {
        try {
            // InternalCRUD.g:154:1: ( ruleData EOF )
            // InternalCRUD.g:155:1: ruleData EOF
            {
             before(grammarAccess.getDataRule()); 
            pushFollow(FOLLOW_1);
            ruleData();

            state._fsp--;

             after(grammarAccess.getDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleData"


    // $ANTLR start "ruleData"
    // InternalCRUD.g:162:1: ruleData : ( ( rule__Data__ValueAssignment ) ) ;
    public final void ruleData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:166:2: ( ( ( rule__Data__ValueAssignment ) ) )
            // InternalCRUD.g:167:2: ( ( rule__Data__ValueAssignment ) )
            {
            // InternalCRUD.g:167:2: ( ( rule__Data__ValueAssignment ) )
            // InternalCRUD.g:168:3: ( rule__Data__ValueAssignment )
            {
             before(grammarAccess.getDataAccess().getValueAssignment()); 
            // InternalCRUD.g:169:3: ( rule__Data__ValueAssignment )
            // InternalCRUD.g:169:4: rule__Data__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Data__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDataAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleData"


    // $ANTLR start "rule__Model__Group__0"
    // InternalCRUD.g:177:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:181:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalCRUD.g:182:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalCRUD.g:189:1: rule__Model__Group__0__Impl : ( 'model' ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:193:1: ( ( 'model' ) )
            // InternalCRUD.g:194:1: ( 'model' )
            {
            // InternalCRUD.g:194:1: ( 'model' )
            // InternalCRUD.g:195:2: 'model'
            {
             before(grammarAccess.getModelAccess().getModelKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getModelKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalCRUD.g:204:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:208:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalCRUD.g:209:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalCRUD.g:216:1: rule__Model__Group__1__Impl : ( ( rule__Model__NameAssignment_1 ) ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:220:1: ( ( ( rule__Model__NameAssignment_1 ) ) )
            // InternalCRUD.g:221:1: ( ( rule__Model__NameAssignment_1 ) )
            {
            // InternalCRUD.g:221:1: ( ( rule__Model__NameAssignment_1 ) )
            // InternalCRUD.g:222:2: ( rule__Model__NameAssignment_1 )
            {
             before(grammarAccess.getModelAccess().getNameAssignment_1()); 
            // InternalCRUD.g:223:2: ( rule__Model__NameAssignment_1 )
            // InternalCRUD.g:223:3: rule__Model__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Model__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalCRUD.g:231:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:235:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // InternalCRUD.g:236:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalCRUD.g:243:1: rule__Model__Group__2__Impl : ( '{' ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:247:1: ( ( '{' ) )
            // InternalCRUD.g:248:1: ( '{' )
            {
            // InternalCRUD.g:248:1: ( '{' )
            // InternalCRUD.g:249:2: '{'
            {
             before(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // InternalCRUD.g:258:1: rule__Model__Group__3 : rule__Model__Group__3__Impl rule__Model__Group__4 ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:262:1: ( rule__Model__Group__3__Impl rule__Model__Group__4 )
            // InternalCRUD.g:263:2: rule__Model__Group__3__Impl rule__Model__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Model__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // InternalCRUD.g:270:1: rule__Model__Group__3__Impl : ( ( rule__Model__AttributesAssignment_3 )* ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:274:1: ( ( ( rule__Model__AttributesAssignment_3 )* ) )
            // InternalCRUD.g:275:1: ( ( rule__Model__AttributesAssignment_3 )* )
            {
            // InternalCRUD.g:275:1: ( ( rule__Model__AttributesAssignment_3 )* )
            // InternalCRUD.g:276:2: ( rule__Model__AttributesAssignment_3 )*
            {
             before(grammarAccess.getModelAccess().getAttributesAssignment_3()); 
            // InternalCRUD.g:277:2: ( rule__Model__AttributesAssignment_3 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCRUD.g:277:3: rule__Model__AttributesAssignment_3
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Model__AttributesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getAttributesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__Model__Group__4"
    // InternalCRUD.g:285:1: rule__Model__Group__4 : rule__Model__Group__4__Impl rule__Model__Group__5 ;
    public final void rule__Model__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:289:1: ( rule__Model__Group__4__Impl rule__Model__Group__5 )
            // InternalCRUD.g:290:2: rule__Model__Group__4__Impl rule__Model__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Model__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4"


    // $ANTLR start "rule__Model__Group__4__Impl"
    // InternalCRUD.g:297:1: rule__Model__Group__4__Impl : ( ( rule__Model__EndpointAssignment_4 )* ) ;
    public final void rule__Model__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:301:1: ( ( ( rule__Model__EndpointAssignment_4 )* ) )
            // InternalCRUD.g:302:1: ( ( rule__Model__EndpointAssignment_4 )* )
            {
            // InternalCRUD.g:302:1: ( ( rule__Model__EndpointAssignment_4 )* )
            // InternalCRUD.g:303:2: ( rule__Model__EndpointAssignment_4 )*
            {
             before(grammarAccess.getModelAccess().getEndpointAssignment_4()); 
            // InternalCRUD.g:304:2: ( rule__Model__EndpointAssignment_4 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCRUD.g:304:3: rule__Model__EndpointAssignment_4
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Model__EndpointAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getEndpointAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4__Impl"


    // $ANTLR start "rule__Model__Group__5"
    // InternalCRUD.g:312:1: rule__Model__Group__5 : rule__Model__Group__5__Impl ;
    public final void rule__Model__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:316:1: ( rule__Model__Group__5__Impl )
            // InternalCRUD.g:317:2: rule__Model__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5"


    // $ANTLR start "rule__Model__Group__5__Impl"
    // InternalCRUD.g:323:1: rule__Model__Group__5__Impl : ( '}' ) ;
    public final void rule__Model__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:327:1: ( ( '}' ) )
            // InternalCRUD.g:328:1: ( '}' )
            {
            // InternalCRUD.g:328:1: ( '}' )
            // InternalCRUD.g:329:2: '}'
            {
             before(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // InternalCRUD.g:339:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:343:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // InternalCRUD.g:344:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // InternalCRUD.g:351:1: rule__Attribute__Group__0__Impl : ( ( rule__Attribute__NameAssignment_0 ) ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:355:1: ( ( ( rule__Attribute__NameAssignment_0 ) ) )
            // InternalCRUD.g:356:1: ( ( rule__Attribute__NameAssignment_0 ) )
            {
            // InternalCRUD.g:356:1: ( ( rule__Attribute__NameAssignment_0 ) )
            // InternalCRUD.g:357:2: ( rule__Attribute__NameAssignment_0 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_0()); 
            // InternalCRUD.g:358:2: ( rule__Attribute__NameAssignment_0 )
            // InternalCRUD.g:358:3: rule__Attribute__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // InternalCRUD.g:366:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:370:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // InternalCRUD.g:371:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // InternalCRUD.g:378:1: rule__Attribute__Group__1__Impl : ( ':' ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:382:1: ( ( ':' ) )
            // InternalCRUD.g:383:1: ( ':' )
            {
            // InternalCRUD.g:383:1: ( ':' )
            // InternalCRUD.g:384:2: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // InternalCRUD.g:393:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:397:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // InternalCRUD.g:398:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // InternalCRUD.g:405:1: rule__Attribute__Group__2__Impl : ( ( rule__Attribute__TypeAssignment_2 ) ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:409:1: ( ( ( rule__Attribute__TypeAssignment_2 ) ) )
            // InternalCRUD.g:410:1: ( ( rule__Attribute__TypeAssignment_2 ) )
            {
            // InternalCRUD.g:410:1: ( ( rule__Attribute__TypeAssignment_2 ) )
            // InternalCRUD.g:411:2: ( rule__Attribute__TypeAssignment_2 )
            {
             before(grammarAccess.getAttributeAccess().getTypeAssignment_2()); 
            // InternalCRUD.g:412:2: ( rule__Attribute__TypeAssignment_2 )
            // InternalCRUD.g:412:3: rule__Attribute__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // InternalCRUD.g:420:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:424:1: ( rule__Attribute__Group__3__Impl )
            // InternalCRUD.g:425:2: rule__Attribute__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // InternalCRUD.g:431:1: rule__Attribute__Group__3__Impl : ( ( rule__Attribute__Group_3__0 )? ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:435:1: ( ( ( rule__Attribute__Group_3__0 )? ) )
            // InternalCRUD.g:436:1: ( ( rule__Attribute__Group_3__0 )? )
            {
            // InternalCRUD.g:436:1: ( ( rule__Attribute__Group_3__0 )? )
            // InternalCRUD.g:437:2: ( rule__Attribute__Group_3__0 )?
            {
             before(grammarAccess.getAttributeAccess().getGroup_3()); 
            // InternalCRUD.g:438:2: ( rule__Attribute__Group_3__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalCRUD.g:438:3: rule__Attribute__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attribute__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__Attribute__Group_3__0"
    // InternalCRUD.g:447:1: rule__Attribute__Group_3__0 : rule__Attribute__Group_3__0__Impl rule__Attribute__Group_3__1 ;
    public final void rule__Attribute__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:451:1: ( rule__Attribute__Group_3__0__Impl rule__Attribute__Group_3__1 )
            // InternalCRUD.g:452:2: rule__Attribute__Group_3__0__Impl rule__Attribute__Group_3__1
            {
            pushFollow(FOLLOW_4);
            rule__Attribute__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_3__0"


    // $ANTLR start "rule__Attribute__Group_3__0__Impl"
    // InternalCRUD.g:459:1: rule__Attribute__Group_3__0__Impl : ( ':' ) ;
    public final void rule__Attribute__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:463:1: ( ( ':' ) )
            // InternalCRUD.g:464:1: ( ':' )
            {
            // InternalCRUD.g:464:1: ( ':' )
            // InternalCRUD.g:465:2: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_3_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_3__0__Impl"


    // $ANTLR start "rule__Attribute__Group_3__1"
    // InternalCRUD.g:474:1: rule__Attribute__Group_3__1 : rule__Attribute__Group_3__1__Impl ;
    public final void rule__Attribute__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:478:1: ( rule__Attribute__Group_3__1__Impl )
            // InternalCRUD.g:479:2: rule__Attribute__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_3__1"


    // $ANTLR start "rule__Attribute__Group_3__1__Impl"
    // InternalCRUD.g:485:1: rule__Attribute__Group_3__1__Impl : ( ( ( rule__Attribute__DataAssignment_3_1 ) ) ( ( rule__Attribute__DataAssignment_3_1 )* ) ) ;
    public final void rule__Attribute__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:489:1: ( ( ( ( rule__Attribute__DataAssignment_3_1 ) ) ( ( rule__Attribute__DataAssignment_3_1 )* ) ) )
            // InternalCRUD.g:490:1: ( ( ( rule__Attribute__DataAssignment_3_1 ) ) ( ( rule__Attribute__DataAssignment_3_1 )* ) )
            {
            // InternalCRUD.g:490:1: ( ( ( rule__Attribute__DataAssignment_3_1 ) ) ( ( rule__Attribute__DataAssignment_3_1 )* ) )
            // InternalCRUD.g:491:2: ( ( rule__Attribute__DataAssignment_3_1 ) ) ( ( rule__Attribute__DataAssignment_3_1 )* )
            {
            // InternalCRUD.g:491:2: ( ( rule__Attribute__DataAssignment_3_1 ) )
            // InternalCRUD.g:492:3: ( rule__Attribute__DataAssignment_3_1 )
            {
             before(grammarAccess.getAttributeAccess().getDataAssignment_3_1()); 
            // InternalCRUD.g:493:3: ( rule__Attribute__DataAssignment_3_1 )
            // InternalCRUD.g:493:4: rule__Attribute__DataAssignment_3_1
            {
            pushFollow(FOLLOW_7);
            rule__Attribute__DataAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getDataAssignment_3_1()); 

            }

            // InternalCRUD.g:496:2: ( ( rule__Attribute__DataAssignment_3_1 )* )
            // InternalCRUD.g:497:3: ( rule__Attribute__DataAssignment_3_1 )*
            {
             before(grammarAccess.getAttributeAccess().getDataAssignment_3_1()); 
            // InternalCRUD.g:498:3: ( rule__Attribute__DataAssignment_3_1 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    int LA5_2 = input.LA(2);

                    if ( (LA5_2==EOF||LA5_2==RULE_ID||LA5_2==13||LA5_2==15) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // InternalCRUD.g:498:4: rule__Attribute__DataAssignment_3_1
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Attribute__DataAssignment_3_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getAttributeAccess().getDataAssignment_3_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_3__1__Impl"


    // $ANTLR start "rule__Endpoint__Group__0"
    // InternalCRUD.g:508:1: rule__Endpoint__Group__0 : rule__Endpoint__Group__0__Impl rule__Endpoint__Group__1 ;
    public final void rule__Endpoint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:512:1: ( rule__Endpoint__Group__0__Impl rule__Endpoint__Group__1 )
            // InternalCRUD.g:513:2: rule__Endpoint__Group__0__Impl rule__Endpoint__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Endpoint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Endpoint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__0"


    // $ANTLR start "rule__Endpoint__Group__0__Impl"
    // InternalCRUD.g:520:1: rule__Endpoint__Group__0__Impl : ( 'endpoint' ) ;
    public final void rule__Endpoint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:524:1: ( ( 'endpoint' ) )
            // InternalCRUD.g:525:1: ( 'endpoint' )
            {
            // InternalCRUD.g:525:1: ( 'endpoint' )
            // InternalCRUD.g:526:2: 'endpoint'
            {
             before(grammarAccess.getEndpointAccess().getEndpointKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getEndpointAccess().getEndpointKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__0__Impl"


    // $ANTLR start "rule__Endpoint__Group__1"
    // InternalCRUD.g:535:1: rule__Endpoint__Group__1 : rule__Endpoint__Group__1__Impl rule__Endpoint__Group__2 ;
    public final void rule__Endpoint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:539:1: ( rule__Endpoint__Group__1__Impl rule__Endpoint__Group__2 )
            // InternalCRUD.g:540:2: rule__Endpoint__Group__1__Impl rule__Endpoint__Group__2
            {
            pushFollow(FOLLOW_9);
            rule__Endpoint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Endpoint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__1"


    // $ANTLR start "rule__Endpoint__Group__1__Impl"
    // InternalCRUD.g:547:1: rule__Endpoint__Group__1__Impl : ( ( rule__Endpoint__TypeAssignment_1 ) ) ;
    public final void rule__Endpoint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:551:1: ( ( ( rule__Endpoint__TypeAssignment_1 ) ) )
            // InternalCRUD.g:552:1: ( ( rule__Endpoint__TypeAssignment_1 ) )
            {
            // InternalCRUD.g:552:1: ( ( rule__Endpoint__TypeAssignment_1 ) )
            // InternalCRUD.g:553:2: ( rule__Endpoint__TypeAssignment_1 )
            {
             before(grammarAccess.getEndpointAccess().getTypeAssignment_1()); 
            // InternalCRUD.g:554:2: ( rule__Endpoint__TypeAssignment_1 )
            // InternalCRUD.g:554:3: rule__Endpoint__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Endpoint__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEndpointAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__1__Impl"


    // $ANTLR start "rule__Endpoint__Group__2"
    // InternalCRUD.g:562:1: rule__Endpoint__Group__2 : rule__Endpoint__Group__2__Impl rule__Endpoint__Group__3 ;
    public final void rule__Endpoint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:566:1: ( rule__Endpoint__Group__2__Impl rule__Endpoint__Group__3 )
            // InternalCRUD.g:567:2: rule__Endpoint__Group__2__Impl rule__Endpoint__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Endpoint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Endpoint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__2"


    // $ANTLR start "rule__Endpoint__Group__2__Impl"
    // InternalCRUD.g:574:1: rule__Endpoint__Group__2__Impl : ( ':' ) ;
    public final void rule__Endpoint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:578:1: ( ( ':' ) )
            // InternalCRUD.g:579:1: ( ':' )
            {
            // InternalCRUD.g:579:1: ( ':' )
            // InternalCRUD.g:580:2: ':'
            {
             before(grammarAccess.getEndpointAccess().getColonKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getEndpointAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__2__Impl"


    // $ANTLR start "rule__Endpoint__Group__3"
    // InternalCRUD.g:589:1: rule__Endpoint__Group__3 : rule__Endpoint__Group__3__Impl ;
    public final void rule__Endpoint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:593:1: ( rule__Endpoint__Group__3__Impl )
            // InternalCRUD.g:594:2: rule__Endpoint__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Endpoint__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__3"


    // $ANTLR start "rule__Endpoint__Group__3__Impl"
    // InternalCRUD.g:600:1: rule__Endpoint__Group__3__Impl : ( ( rule__Endpoint__ValueAssignment_3 ) ) ;
    public final void rule__Endpoint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:604:1: ( ( ( rule__Endpoint__ValueAssignment_3 ) ) )
            // InternalCRUD.g:605:1: ( ( rule__Endpoint__ValueAssignment_3 ) )
            {
            // InternalCRUD.g:605:1: ( ( rule__Endpoint__ValueAssignment_3 ) )
            // InternalCRUD.g:606:2: ( rule__Endpoint__ValueAssignment_3 )
            {
             before(grammarAccess.getEndpointAccess().getValueAssignment_3()); 
            // InternalCRUD.g:607:2: ( rule__Endpoint__ValueAssignment_3 )
            // InternalCRUD.g:607:3: rule__Endpoint__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Endpoint__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getEndpointAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__Group__3__Impl"


    // $ANTLR start "rule__Models__ElementsAssignment"
    // InternalCRUD.g:616:1: rule__Models__ElementsAssignment : ( ruleModel ) ;
    public final void rule__Models__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:620:1: ( ( ruleModel ) )
            // InternalCRUD.g:621:2: ( ruleModel )
            {
            // InternalCRUD.g:621:2: ( ruleModel )
            // InternalCRUD.g:622:3: ruleModel
            {
             before(grammarAccess.getModelsAccess().getElementsModelParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelsAccess().getElementsModelParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Models__ElementsAssignment"


    // $ANTLR start "rule__Model__NameAssignment_1"
    // InternalCRUD.g:631:1: rule__Model__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Model__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:635:1: ( ( RULE_ID ) )
            // InternalCRUD.g:636:2: ( RULE_ID )
            {
            // InternalCRUD.g:636:2: ( RULE_ID )
            // InternalCRUD.g:637:3: RULE_ID
            {
             before(grammarAccess.getModelAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__NameAssignment_1"


    // $ANTLR start "rule__Model__AttributesAssignment_3"
    // InternalCRUD.g:646:1: rule__Model__AttributesAssignment_3 : ( ruleAttribute ) ;
    public final void rule__Model__AttributesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:650:1: ( ( ruleAttribute ) )
            // InternalCRUD.g:651:2: ( ruleAttribute )
            {
            // InternalCRUD.g:651:2: ( ruleAttribute )
            // InternalCRUD.g:652:3: ruleAttribute
            {
             before(grammarAccess.getModelAccess().getAttributesAttributeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getModelAccess().getAttributesAttributeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__AttributesAssignment_3"


    // $ANTLR start "rule__Model__EndpointAssignment_4"
    // InternalCRUD.g:661:1: rule__Model__EndpointAssignment_4 : ( ruleEndpoint ) ;
    public final void rule__Model__EndpointAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:665:1: ( ( ruleEndpoint ) )
            // InternalCRUD.g:666:2: ( ruleEndpoint )
            {
            // InternalCRUD.g:666:2: ( ruleEndpoint )
            // InternalCRUD.g:667:3: ruleEndpoint
            {
             before(grammarAccess.getModelAccess().getEndpointEndpointParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleEndpoint();

            state._fsp--;

             after(grammarAccess.getModelAccess().getEndpointEndpointParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__EndpointAssignment_4"


    // $ANTLR start "rule__Attribute__NameAssignment_0"
    // InternalCRUD.g:676:1: rule__Attribute__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Attribute__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:680:1: ( ( RULE_ID ) )
            // InternalCRUD.g:681:2: ( RULE_ID )
            {
            // InternalCRUD.g:681:2: ( RULE_ID )
            // InternalCRUD.g:682:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_0"


    // $ANTLR start "rule__Attribute__TypeAssignment_2"
    // InternalCRUD.g:691:1: rule__Attribute__TypeAssignment_2 : ( RULE_ID ) ;
    public final void rule__Attribute__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:695:1: ( ( RULE_ID ) )
            // InternalCRUD.g:696:2: ( RULE_ID )
            {
            // InternalCRUD.g:696:2: ( RULE_ID )
            // InternalCRUD.g:697:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__TypeAssignment_2"


    // $ANTLR start "rule__Attribute__DataAssignment_3_1"
    // InternalCRUD.g:706:1: rule__Attribute__DataAssignment_3_1 : ( ruleData ) ;
    public final void rule__Attribute__DataAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:710:1: ( ( ruleData ) )
            // InternalCRUD.g:711:2: ( ruleData )
            {
            // InternalCRUD.g:711:2: ( ruleData )
            // InternalCRUD.g:712:3: ruleData
            {
             before(grammarAccess.getAttributeAccess().getDataDataParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleData();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getDataDataParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__DataAssignment_3_1"


    // $ANTLR start "rule__Endpoint__TypeAssignment_1"
    // InternalCRUD.g:721:1: rule__Endpoint__TypeAssignment_1 : ( RULE_ID ) ;
    public final void rule__Endpoint__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:725:1: ( ( RULE_ID ) )
            // InternalCRUD.g:726:2: ( RULE_ID )
            {
            // InternalCRUD.g:726:2: ( RULE_ID )
            // InternalCRUD.g:727:3: RULE_ID
            {
             before(grammarAccess.getEndpointAccess().getTypeIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEndpointAccess().getTypeIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__TypeAssignment_1"


    // $ANTLR start "rule__Endpoint__ValueAssignment_3"
    // InternalCRUD.g:736:1: rule__Endpoint__ValueAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Endpoint__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:740:1: ( ( RULE_STRING ) )
            // InternalCRUD.g:741:2: ( RULE_STRING )
            {
            // InternalCRUD.g:741:2: ( RULE_STRING )
            // InternalCRUD.g:742:3: RULE_STRING
            {
             before(grammarAccess.getEndpointAccess().getValueSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEndpointAccess().getValueSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endpoint__ValueAssignment_3"


    // $ANTLR start "rule__Data__ValueAssignment"
    // InternalCRUD.g:751:1: rule__Data__ValueAssignment : ( RULE_ID ) ;
    public final void rule__Data__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCRUD.g:755:1: ( ( RULE_ID ) )
            // InternalCRUD.g:756:2: ( RULE_ID )
            {
            // InternalCRUD.g:756:2: ( RULE_ID )
            // InternalCRUD.g:757:3: RULE_ID
            {
             before(grammarAccess.getDataAccess().getValueIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDataAccess().getValueIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Data__ValueAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000A010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000020L});

}